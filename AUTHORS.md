# Contributors

Shapley-effects is the outcome of a project at the [CEMRACS 2017 summer school](http://smai.emath.fr/cemracs/cemracs17/). The main developers of this library are:

- Nazih Benoumechiara,
- Kévin Elie-Dit-Cosaque.

We also thank those that contributed to the establishment of this library:

- Bertrand Iooss,
- Véronique Maume-Deschamps,
- Clémentine Prieur.
- Roman Sueur,

A special thanks to the authors of the R package [sensitivity](https://cran.r-project.org/web/packages/sensitivity/index.html) which helps us validate our results.