# Release history

## Version 0.1

### New features

- Shapley effect computation.
- Sobol' indices computation.
- Kriging model from sklearn, GPflow and OpenTURNS are available.
- Random Forest model from sklearn.